# Git for TFS users
Lab 02: Working with SSH keys

---

# Tasks

 - Creating a SSH key
 
 - Configure a new SSH key
 
 - Clone with SSH

---

## Creating a SSH key

 - Create a new ssh key using the command:
```
$ ssh-keygen -t rsa -C "your_email@example.com"
```

 - This creates a new ssh key, using the provided email as a label:
```
Generating public/private rsa key pair.
```

 - You will be prompted to "Enter a file in which to save the key," press enter for the default location:
```
Enter file in which to save the key (/c/Users/leonj/.ssh/id_rsa):
```

 - You will be asked for a passphrase, leave it empty and press enter:
```
Enter passphrase (empty for no passphrase):
```

 - You will be asked for a passphrase confirmation, leave it empty and press enter:
```
Enter same passphrase again:
```

 - If you see a message like the below, your pair keys were successfully created:
```
Your identification has been saved in /c/Users/leonj/.ssh/id_rsa.
Your public key has been saved in /c/Users/leonj/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:TTvfIfTaP5xLm0ZM9xl+qBgoGgAzHYZrIhqHPPrF8Ko your_email@example.com
The key's randomart image is:
+---[RSA 4096]----+
| oo.             |
|=..              |
|.*        . .    |
|*++      o o . o.|
|*o.=    S.+ . *.=|
|o   = . . .o =.*o|
| . o o .   oo.=.o|
|  o .     . . .*o|
|E.            .++|
+----[SHA256]-----+
```

 - Move to the path where the keys were created (by default):
```
$ cd ~/.ssh
```

 - Inspect the directory to see its contents:
```
$ ls -l
```

 - There you will find the private key (id_rsa) and the public key (id_rsa.pub)
```
-rw-r--r-- 1 leonj 197609 3243 May 11 12:04 id_rsa
-rw-r--r-- 1 leonj 197609  748 May 11 12:04 id_rsa.pub
```

 - Let's see the content of the private key
```
$ cat id_rsa
```

 - Now let's look at the public key (copy the content, we will use it later):
```
$ cat id_rsa.pub
```

---

## Configure a new SSH key in TFS

 - Add the public key to TFS:

&nbsp;
<img alt="Image 1.1" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/use-ssh-authentication/ssh_profile_access.png?view=azure-devops" border="1">
&nbsp;

 - Select SSH Public Keys , then select Add:

&nbsp;
<img alt="Image 1.2" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/use-ssh-authentication/ssh_accessing_security_key.png?view=azure-devops" border="1">
&nbsp;

 - Copy the contents of the public key (for example, id_rsa.pub) that you generated into the Key Data field:

&nbsp;
<img alt="Image 1.2" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/use-ssh-authentication/ssh_key_input.png?view=azure-devops" border="1">
&nbsp;

 - Give the key a useful description so that you can remember it later

 
## Clone the Git repository with SSH

 - Copy the SSH clone URL from the web portal:

&nbsp;
<img alt="Image 1.2" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/use-ssh-authentication/ssh_clone_url.png?view=azure-devops" border="1">
&nbsp;

 - Run git clone from the command prompt:
```
$ git clone <repository-url>
```
